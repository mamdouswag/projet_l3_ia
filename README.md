1.  Synchronisation avec git:

    Dans ton dossier ou placer le projet : **click droit > git bash**

    `git init`
    `git remote add origin https://gitlab.com/mamdouswag/projet_l3_ia`
    `git branch -M main`
    `git clone https://gitlab.com/mamdouswag/projet_l3_ia`
    `git pull`

2.  Prerequis a Installer:

    - python 3

    - pip - installer de librairies

    - black - formatteur de texte
      installer avec : `pip install black`
      une fois fini de coder, faire : `black -l 88 "mon_fichier.py"`

3.  Ajout de fonctionnalitees:

    - dev est la branche principale, a ne pas coder dedans

    - pour ajouter des nv fonctions, faire une copie du dossier 'dev' dans branches
      exemple: "ma nouvelle fonction"

    ├───branches
    │ ├───dev
    │ └───"ma nouvelle fonction"
    ├───tags
    └───trunk

    - une fois que ma nouvelle branche fonctionne sans erreurs, merge dans dev

    - trunk = la version finale
