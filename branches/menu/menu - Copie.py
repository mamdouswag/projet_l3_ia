import pygame

"""
https://github.com/ChristianD37/YoutubeTutorials/blob/master/Menu%20System/menu.py
"""


class Menu:
    def __init__(self, game):
        self.game = game
        self.mid_w = self.game.DISPLAY_W / 2
        self.mid_h = self.game.DISPLAY_H / 2
        self.run_display = True
        self.cursor_rect = pygame.Rect(0, 0, 20, 20)
        self.offset = -100

    def draw_cursor(self):
        self.game.draw_text("*", 15, self.cursor_rect.x, self.cursor_rect.y)

    def blit_screen(self):
        self.game.window.blit(self.game.display, (0, 0))
        pygame.display.update()
        self.game.reset_keys()


class MainMenu:
    def __init__(self, game):
        Menu.__init__(self, game)
        self.state = "pvp"
        self.pvpX = self.mid_w
        self.pvpY = self.mid_h + 30
        self.pveX = self.mid_w
        self.pveY = self.mid_h + 50
        self.cursor_rect.midtop = (self.pvpX + self.offset, self.pvpY)

    def display_menu(self):
        self.run_display = True

        while self.run_display:
            self.game.check_events()
            self.check_input()
            self.game.display.fill(self.game.BLACK)
            self.game.draw_text(
                "Pokemon",
                20,
                self.game.DISPLAY_W / 2,
                self.game.DISPLAY_H / 8 * 2,
            )
            self.game.draw_text(
                "PvP",
                20,
                self.game.DISPLAY_W / 2,
                self.game.DISPLAY_H / 8 * 4,
            )
            self.game.draw_text(
                "PvE",
                20,
                self.game.DISPLAY_W / 2,
                self.game.DISPLAY_H / 8 * 5,
            )
            self.draw_cursor()
            self.blit_screen()

    def move_cursor(self):
        if self.game.DOWN_KEY:
            if self.state == "pvp":
                self.cursor_rect.midtop = (self.pveX + self.offset, self.pveY)
                self.state = "pve"

        elif self.game.UP_KEY:
            if self.state == "pve":
                self.cursor_rect.midtop = (self.pvpX + self.offset, self.pvpY)
                self.state = "pvp"

    def check_input(self):
        self.move_cursor()
        if self.game.START_KEY:
            self.game.playing == True
            self.run_display = False

    def MenuChoixDifficulte(Menu):
        return None

    def Menu_SelectionPokemon(Menu):
        return None
