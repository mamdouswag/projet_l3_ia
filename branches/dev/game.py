import pygame


class Game:
    def __init__(self) -> None:
        pygame.init()
        self.running: bool = True
        self.playing: bool = False
        self.UP_KEY: bool = False
        self.DOWN_KEY: bool = False
        self.ENTER_KEY: bool = False
        self.BACK_KEY: bool = False
        self.screen_width: int = 1000
        self.screen_higth: int = 600
        self.display = pygame.Surface((self.screen_width, self.screen_higth))
        self.window = pygame.display.set_mode((self.screen_width, self.screen_higth))
        self.font_name = pygame.font.get_default_font()
        self.BLACK = (0, 0, 0)
        self.WHITE = (255, 255, 255)

    def game_loop(self) -> None:
        while self.playing:
            self.check_events()

            if self.ENTER_KEY:
                self.playing = False

            self.display.fill(self.BLACK)
            # self.draw_text('test', 20, self.screen_width/2 , self.screen_higth/6)
            self.draw_text(
                "Choisir 3 pokemons",
                20,
                self.screen_width / 2,
                self.screen_higth / 8 * 2,
            )
            self.draw_text("Feu", 20, self.screen_width / 2, self.screen_higth / 8 * 4)
            self.draw_text("Eau", 20, self.screen_width / 2, self.screen_higth / 8 * 5)
            self.draw_text(
                "Plante", 20, self.screen_width / 2, self.screen_higth / 8 * 6
            )
            self.window.blit(self.display, (0, 0))
            pygame.display.update()
            self.reset_keys()

    def check_events(self) -> None:
        for event in pygame.event.get():
            # Verifie si le joueur quitte le jeu
            if event.type == pygame.QUIT:
                self.running = False
                self.playing = False

            if event.type == pygame.KEYDOWN:
                # Verifie si le joueur appuie sur ENTRER
                if event.type == pygame.K_RETURN:
                    self.ENTER_KEY = True

                # Verifie si le joueur appuie sur RETOUR
                if event.type == pygame.K_BACKSPACE:
                    self.BACK_KEY = True

                # Verifie si le joueur appuie sur HAUT
                if event.type == pygame.K_UP:
                    self.UP_KEY = True

                # Verifie si le joueur appuie sur BAS
                if event.type == pygame.K_DOWN:
                    self.DOWN_KEY = True

    def reset_keys(self) -> None:
        self.UP_KEY: bool = False
        self.DOWN_KEY: bool = False
        self.ENTER_KEY: bool = False
        self.BACK_KEY: bool = False

    def draw_text(self, text_to_draw, size_of_text, coordonnee_X, coordonnee_Y) -> None:
        font = pygame.font.Font(self.font_name, size_of_text)
        text_surface = font.render(text_to_draw, True, self.WHITE)
        text_rectangle = text_surface.get_rect()
        text_rectangle.center = (coordonnee_X, coordonnee_Y)
        self.display.blit(text_surface, text_rectangle)
