Regles du jeu :

1. Choisir 3 pokemons :
    un pokemon a :
        - 5 HP
        - une vitesse
        - un type (feu, eau ou plante)
        - un attaque qui inflige 1*multiplier degats a son adversaire, a usage infini
        - un bouclier qui bloque la prochaine attaque adverse, a usage unique

2. types :
    - Feu prend :
        x0.5 dmg par Plante
        x1 dmg par Feu
        x2 dmg par Eau

    - Eau prend :
        x0.5 dmg par Feu
        x1 dmg par Eau
        x2 dmg par Plante

    - Plante prend :
        x0.5 dmg par Eau
        x1   dmg par Plante
        x2   dmg par Feu

3. Deroulement du combat:
    - Chaque joueurs selectionnes 3 pokemons dans son equipe, un pokemon du meme type peux etre selectionne plusieurs fois
    - Le jeu se deroule en tour par tour
    - Chaque joueurs a le choix entre 3 actions a chaque tours : Attaquer, Defendre (si le bouclier est disponible) et Changer de Pokemon
    - Le pokemon avec la plus grande vitesse commence le tour, en cas d'egalite, c'est rng
    - Un swap termine le tour du joueur

4. Fin de partie :
    Quand tous les pokemons du joueur ou de son adversaire sont a 0 HP